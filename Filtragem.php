<?php
if($_GET['idUsu']!==null){
	require_once("Controle/RegistroController.class.php");
	$usu = $_GET['idUsu'];
	$filtro = (isset($_GET['filtro'])) ?$_GET['filtro'] :null;	
	$valor = $_GET["valor"];
	$reg = new RegistroController();
	$detalhes2 = [];
	if($filtro!="nenhum"){
		$regC = $reg->consultarRegistrosFiltro((integer)$_GET['idUsu'],$filtro,$valor);
		if($regC){
				$cont2 = 0;	
				foreach ($regC as $rL) {
					$titulo = $rL->getTitulo();
					$valor = $rL->getValor();
					$tpRegistro = "plus.png";
					$tipoRegistro = "Entrada";
					if($rL->getTipoRegistro()=="saida"){
						$valor = $valor*(-1);
						$tpRegistro = "less.png";
						$tipoRegistro = "Saída";
					}
					$valorNormal = $valor;
					$valor = number_format($valor, 2, ",", ".");

					$idForma = $usu;
					$meioDetalhado = "";
					if($rL->getTipoPagamento()=="carteira"){
						$meio = "Carteira";
						$meioDetalhado = "Carteira";
					}else if($rL->getTipoPagamento()=="conta"){
						$meio = "Conta Bancaria";
						$meioDetalhado = "Conta Bancaria ".$rL->getConta()->getConta()." - ".$rL->getConta()->getAgencia()->getNome();
						$idForma = $rL->getConta()->getConta();
					}else{
						$meio = "Cartão";
						if($rL->getCartao()->getContaBancaria()->getConta()!=null){
							$meioDetalhado = "Cartão ".$rL->getCartao()->getId()." - ".$rL->getCartao()->getContaBancaria()->getAgencia()->getNome();
						}else{
							$meioDetalhado = "Cartão ".$rL->getCartao()->getId()." - Sem Banco";
						}
						
						$idForma = $rL->getCartao()->getId();
					}
					$dataReg = date('d-m-Y à\s H:i:s',strtotime($rL->getDataRegistro()));
					$dataOp = date('d-m-Y',strtotime($rL->getDataOperacao()));
					$dataVen = date('d-m-Y',strtotime($rL->getDataVencimento()));
					$dataVen = ($rL->getDataVencimento()!=null) ?"Dia de Vencimento: ".date('d-m',strtotime($rL->getDataVencimento()))." <> Até ".$dataVen :"";
					echo "<tr>";
					echo "<td><img style='width:15px;height:15px' src='imgs/{$tpRegistro}'></td>";
					echo "<td>{$titulo}</td>";
					echo "<td>R$ {$valor}</td>";
					echo "<td>{$meio}</td>";
					echo "<td>{$dataReg}</td>";
					echo "<td><a class='uk-icon-button verRegistro2' onclick='verRegistroFiltro({$cont2})'><span uk-icon='icon: search'></span></a>";
					echo "<a onclick='deletarRegistro({$rL->getId()},{$rL->getTipoPagamento()},{$valorNormal},{$idForma})' class='uk-icon-button'><span uk-icon='icon: close'></span></a></td>";
					echo "</tr>";



					$detalhes2[$cont2] = "".
					"<h2>{$titulo} <a class='uk-icon-button btnPDF2' id='{$cont2}'><span uk-icon='icon: download'></span></a></h2>".
					"<hr></hr>".
					"<p>Valor: R$ {$valor}</p>".
					"<p>Tipo de registro: {$tipoRegistro}</p>".
					"<p>Meio: {$meioDetalhado}</p>".
					"<p>Data de registro: {$dataReg}</p>".
					"<p>Data de operação: {$dataOp}</p>".
					"<p>{$dataVen}</p>".
					"<hr class='uk-divider-icon'></hr>".
					"<p>{$rL->getDesc()}</p>";
					

					$cont2+=1;
				}
				
		}else{
			echo "<h1>Sem resultados.</h1>";
		}
	}else{
		$regC = $reg->consultarRegistros((integer)$_GET['idUsu']);
		if($regC){
			$cont2 = 0;	
			foreach ($regC as $rL) {
				$titulo = $rL->getTitulo();
				$valor = $rL->getValor();
				$tpRegistro = "plus.png";
				$tipoRegistro = "Entrada";
				if($rL->getTipoRegistro()=="saida"){
					$valor = $valor*(-1);
					$tpRegistro = "less.png";
					$tipoRegistro = "Saída";
				}
				$valorNormal = $valor;
				$valor = number_format($valor, 2, ",", ".");

				$idForma = $usu;
				$meioDetalhado = "";
				if($rL->getTipoPagamento()=="carteira"){
					$meio = "Carteira";
					$meioDetalhado = "Carteira";
				}else if($rL->getTipoPagamento()=="conta"){
					$meio = "Conta Bancaria";
					$meioDetalhado = "Conta Bancaria ".$rL->getConta()->getConta()." - ".$rL->getConta()->getAgencia()->getNome();
					$idForma = $rL->getConta()->getConta();
				}else{
					$meio = "Cartão";
					if($rL->getCartao()->getContaBancaria()->getConta()!=null){
						$meioDetalhado = "Cartão ".$rL->getCartao()->getId()." - ".$rL->getCartao()->getContaBancaria()->getAgencia()->getNome();
					}else{
						$meioDetalhado = "Cartão ".$rL->getCartao()->getId()." - Sem Banco";
					}
					
					$idForma = $rL->getCartao()->getId();
				}
				$dataReg = date('d-m-Y à\s H:i:s',strtotime($rL->getDataRegistro()));
				$dataOp = date('d-m-Y',strtotime($rL->getDataOperacao()));
				$dataVen = date('d-m-Y',strtotime($rL->getDataVencimento()));
				$dataVen = ($rL->getDataVencimento()!=null) ?"Dia de Vencimento: ".date('d-m',strtotime($rL->getDataVencimento()))." <> Até ".$dataVen :"";
				echo "<tr>";
				echo "<td><img style='width:15px;height:15px' src='imgs/{$tpRegistro}'></td>";
				echo "<td>{$titulo}</td>";
				echo "<td>R$ {$valor}</td>";
				echo "<td>{$meio}</td>";
				echo "<td>{$dataReg}</td>";
				echo "<td><a class='uk-icon-button verRegistro2' onclick='verRegistroFiltro({$cont2})'><span uk-icon='icon: search'></span></a>";
				echo "<a onclick='deletarRegistro({$rL->getId()},{$rL->getTipoPagamento()},{$valorNormal},{$idForma})' class='uk-icon-button'><span uk-icon='icon: close'></span></a></td>";
				echo "</tr>";



				$detalhes[$cont2] = "".
				"<h2>{$titulo} <a class='uk-icon-button btnPDF2' id='{$cont2}'><span uk-icon='icon: download'></span></a></h2>".
				"<hr></hr>".
				"<p>Valor: R$ {$valor}</p>".
				"<p>Tipo de registro: {$tipoRegistro}</p>".
				"<p>Meio: {$meioDetalhado}</p>".
				"<p>Data de registro: {$dataReg}</p>".
				"<p>Data de operação: {$dataOp}</p>".
				"<p>{$dataVen}</p>".
				"<hr class='uk-divider-icon'></hr>".
				"<p>{$rL->getDesc()}</p>";
				
			
				$cont2+=1;
			}
			
		}else{
			echo "<h1>Sem resultados.</h1>";
		}
	}
	$detalhes2String = implode("|", $detalhes2);
}
?>
<script>
	var detalhes2String = "<?php echo $detalhes2String ?>";
	var detalhes2 = detalhes2String.split("|");



			function savePDF(codigoHTML) {
			  var doc = new jsPDF('portrait', 'pt', 'a4'),
			      data = new Date();
			  margins = {
			    top: 40,
			    bottom: 60,
			    left: 40,
			    width: 1000
			  };
			  doc.fromHTML(codigoHTML,
			               margins.left, // x coord
			               margins.top, { pagesplit: true },
			               function(dispose){
			    doc.save("registro - "+data.getDate()+"/"+(data.getMonth()+1)+"/"+data.getFullYear()+".pdf");
			  });
			}

		    $('.verRegistro2').click(function(){
		        $(".btnPDF2").click(function(event) {
		        	var det = detalhes2[$(this).attr('id')];
		        	var detalhesPDF = "<img src='imgs/money.png' width='40px'><h1>yourMoney</h1><hr>"+det;
		            savePDF(detalhesPDF);
		            alertify.closeAll();
		        });
			});
</script>