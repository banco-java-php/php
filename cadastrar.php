<?php
    header("Content-type: application/json");
    if (file_exists("Modelo/Usuario.class.php") ) {
        require_once("Modelo/Usuario.class.php");
    }else{
        echo "Classe Usuario nao encontrada -> Cadastrar";
        die();
    }
    if(file_exists("Controle/UsuarioController.class.php")){
        require_once("Controle/UsuarioController.class.php");
    }else{
        echo "Classe UsuarioControle nao encontrada -> Cadastrar";
        die();
    }
    $usuario = new Usuario();
    $usuarioControle = new UsuarioController();
    $usuario->setNome($_POST['nome']);
    $Cifra =  'AES-256-CBC';
    $Chave =  "I�qeBS���yڭJEFf8��/3h ���";
    $IV = random_bytes(openssl_cipher_iv_length($Cifra)); 
    $TextoCifrado = openssl_encrypt($_POST['senha'], $Cifra, $Chave, OPENSSL_RAW_DATA, $IV);
    $senhaCriptografada = base64_encode($IV.$TextoCifrado);
    $usuario->setSenha($senhaCriptografada);
    $usuario->setEmail($_POST['email']);
    if(!$usuarioControle->verificarEmail($usuario->getEmail())){
        if($usuario->getEmail()){
            if($usuario->getNome()){
                if($usuario->getSenha()){
                    if($usuarioControle->inserir($usuario)){
                        $mensagem = array("codigo"=>1,"success"=>array("mensagem"=>"Faça o login para começar a gerenciar o seu dinheiro no yourMoney."));
                        echo json_encode($mensagem);
                        die();
                    }else{
                        $mensagem = array("codigo"=> 0,"erro"=>array("campo"=>"banco", "mensagem"=>"Erro no servidor."));
                        echo json_encode($mensagem);
                        die();
                    }
                }else{
                    $mensagem = array("codigo"=>0,"erro"=>array("campo"=>"senha", "mensagem"=>"Senha inválida."));
                    echo json_encode($mensagem);
                    die();
                }
            }else{
                $mensagem = array("codigo"=>0,"erro"=>array("campo"=>"nome", "mensagem"=>"Nome inválido."));
            echo json_encode($mensagem);
            die();
            }
        }else{
            $mensagem = array("codigo"=>0,"erro"=>array("campo"=>"email", "mensagem"=>"Email inválido."));
            echo json_encode($mensagem);
            die();
        }
    }else{
        $mensagem = array("codigo"=>0,"erro"=>array("campo"=>"email", "mensagem"=>"Email já cadastrado."));
        echo json_encode($mensagem);
        die();
    }
    
?>