
var carteira =	
			"<form action='AdicionarCarteira.php' method='post'>" +
			"<label>Valor</label><br><input class='uk-input' type='text' name='carteira' placeholder='carteira' ><br><br>" +
			"<input class='uk-button uk-button-secondary' type='submit' value='Adicionar'/>" +
			"</form>"; 

var formConta =
			"<form action='AdicionarConta.php' method='post'>" +
			"<label>Conta</label><br><input class='uk-input' type='text' name='conta' placeholder='Número de conta' ><br><br>" +
			"<label>Valor Atual</label><br><input class='uk-input' id='valor1' onclick='maskValor('valor1')' type='text' name='valor' placeholder='Valor' ><br><br>" +
			"<label>Tipo de Conta</label><br><select class='uk-select' name='tipoConta' ><option value=''>Selecionar</option><option value='corrente'>Corrente</option><option value='poupanca'>Poupança</option></select><br><br>" +
			"<label>Instituição Financeira</label><br><select class='uk-select' name='agencia' ><option value=''>Selecionar</option><option value='1'>Banco do Brasil</option><option value='2'>Santander</option><option value='3'>Caixa Econômica Federal</option><option value='4'>Itaú</option><option value='5'>Bradesco</option></select><br><br>" +
			"<input class='uk-button uk-button-secondary' type='submit' value='Adicionar'/>" +
			"</form>";

var formCartao =
			"<form action='AdicionarCartao.php' method='post'>" +
			"<label>Número</label><br><input id='cartao' class='uk-input' type='text' name='id' placeholder='Número do seu Cartão' ><br><br>" +
			"<label>Limite</label><br><input onclick='maskValor('limite')' class='uk-input' id='limite' type='text' name='limite' placeholder='Limite' ><br>" +
			"<label><input type='checkbox' onchange='verificarLimite()' class='uk-checkbox' name='apenasDebito' id='apenasDebito'> Apenas Cartão de Débito</label><br><br>" +
			"<label>Conta</label><br><select class='uk-select' name='conta' ><option value=''>Selecionar</option><option value='0' id='semConta'>Sem Conta Bancária</option>"+contasCartao+"</select><br><br>" +
			"<input class='uk-button uk-button-secondary' type='submit' value='Adicionar'/>" +
			"</form>";

var formRegistro =
			"<form class='uk-grid-small' action='AdicionarRegistro.php' method='post' uk-grid>" + 
			"<div class='uk-width-1-2@s'><label>Tipo de Registro</label><br><select class='uk-select' id='tipoRegistros' onchange='mudarForma();mudarDataOperacao();mudarRepeticao();mudarParcelamento();mudarFixo();mudarTipoRegistro()' name='tipoRegistros' ><option value='saida'>Saída</option><option value='entrada'>Entrada</option></select></div>" +
			"<div class='uk-width-1-2@s'><label>Forma</label><br><select class='uk-select' id='forma' onchange='mudarForma();mudarRepeticao()' name='forma' ><option value='carteira'>Carteira</option><option value='conta'>Conta Bancária</option><option value='cartao' id='formaCartao'>Cartão de Crédito</option></select></div>" +
			"<div class='uk-width-1-1'><label>Título</label><br><input class='uk-input' type='text' name='titulo' placeholder='Título de seu registro' ></div>" +
			"<div class='uk-width-1-2@s'><label>Valor</label><br><input class='uk-input' type='text' id='valor4' onclick='maskValor('valor4')' id='valor' name='valor' placeholder='Valor' ></div>" +
			"<br><div id='fieldFix' class='uk-width-1-2@s' style='display:none'></div>" +
			"<br><div id='fieldConta' class='uk-width-1-2@s' style='display:none'><label>Conta</label><br><select class='uk-select' name='conta' ><option value=''>Selecionar</option>"+contas+"</select></div>" +
			"<br><div id='fieldCartao' class='uk-width-1-2@s' style='display:none'><label>Cartão</label><br><select class='uk-select' name='cartao' ><option value=''>Selecionar</option>"+cartoes+"</select></div>" +
			"<div id='fieldTipoCartao' class='uk-width-1-2@s' style='display:none'><label>Procedimento</label><br><select class='uk-select' onchange='mudarProcedimento()' name='tipoCartao' id='procedimento'><option value='debito'>Débito</option><option value='credito'>Crédito</option></select></div>" +
			"<div class='uk-width-1-2@s'><label>Data da Operação</label><br><select class='uk-select' id='dataO' onchange='mudarDataOperacao()' name='dataO' ><option value='hoje'>Hoje</option><option value='especifico'>Data..</option></select></div>" +
			"<div id='fieldDataOperacao' class='uk-width-1-2@s' style='display:none'><label>Data</label><br><input class='uk-input' type='date' name='dataOperacao'></div>" +
			"<div id='fieldRepeticao' class='uk-width-1-2@s' style='display:none'><label>Repetição</label><br><select class='uk-select' id='repeticao' onchange='mudarRepeticao();mudarFixo();mudarParcelamento()' name='repeticao' ><option value='unico'>Única</option><option value='parcelamento' id='repetParcelamento'>Parcelamento</option><option value='fixa'>Fixa</option></select></div>" +
			"<div id='fieldRepeticaoCarteira' class='uk-width-1-2@s' ><label>Repetição</label><br><select class='uk-select' onchange='mudarParcelamento();mudarFixo();mudarRepeticao()' id='repeticaoCarteira' name='repeticaoCarteira' ><option value='unico'>Única</option><option value='fixa'>Fixa</option></select></div>" +
			"<div id='fieldParcelamento' class='uk-width-1-2@s' style='display:none'><label>Parcelamento</label><br><select class='uk-select' onchange='mudarRepeticao()' id='parcelamento' name='parcelamento' ><option value=''>Selecionar</option><option value='novo'>Novo +</span></option>"+parcelamentos+"</select></div>" +
			"<div id='fieldFixo' class='uk-width-1-2@s' style='display:none'><label>Fixo</label><br><select class='uk-select' name='fixo' ><option value=''>Selecionar</option><option value='novo'>Novo +</span></option>"+registrosFixos+"</select></div>" +
			"<div id='fieldDataVencimento' class='uk-width-1-2@s' style='display:none'><label>Qntd Parcelas Mensais </label><br><input class='uk-input' type='text' name='dataVencimento' placeholder='Quantidade de parcelas'></div>" +
			"<div class='uk-width-1-1'><label>Descrição</label><br><textarea class='uk-textarea' rows='6' style='resize:none' name='descricao' placeholder='Descrição de seu registro'></textarea></div>" +
			"<div class='uk-width-1-1'><input class='uk-button uk-button-secondary' type='submit' value='Enviar'/></div>" +
			"</form>";


