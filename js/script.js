//funções
function verificarSpan() {
    var email = document.getElementById("spanEmail");
    var senha = document.getElementById("spanSenha");
    var senha2 = document.getElementById("spanSenha2");
    var nome = document.getElementById("spanNome");
    if (nome.innerHTML != "" || senha.innerHTML != "" || senha2.innerHTML != "" || email.innerHTML != "") {
        return true;
    } else {
        return false;
    }
}
function verificarCampos() {
    var email = document.getElementById("email");
    var senha = document.getElementById("senha");
    var senha2 = document.getElementById("senha2");
    var nome = document.getElementById("nome");
    if (nome.value != "" || senha.value != "" || senha2.value != "" || email.value != "") {
        return false;
    } else {
        return true;
    }
}
function spanEmail(span, email) {
    if (span.innerHTML == "Email inválido" || span.innerHTML == "Email já cadastrado") {
        email.style.borderColor = "red";
    } else {
        email.style.borderColor = "#77e610f8";
    }
}
function desativarBotao(){
    var botao = document.getElementById("botao");
    if(verificarCampos() || verificarSpan()){
        if(botao.getAttribute("disabled") == null){
            botao.setAttribute("disabled", "");
        }
    }else{
        if(botao.getAttribute("disabled") == ""){
            botao.removeAttribute("disabled");
        }
    }
}
//email
var email = document.getElementById("email");
if (email != undefined) {
    var span = document.getElementById("spanEmail");
    email.oninput = function () {
        requisitarArquivo(span, "verificarEmail.php?email=" + email.value);
    }
    email.onkeyup = function () {
        spanEmail(span, email);
    }
    email.onblur = function () {
        spanEmail(span, email);
    }

}
//senhas
var senha = document.getElementById("senha");
var senha2 = document.getElementById("senha2");
if (senha != undefined && senha2 != undefined) {
    var spanSenha = document.getElementById("spanSenha");
    senha.onblur = function () {

        if (senha.value.length < 8) {
            senha.style.borderColor = "red";
            spanSenha.innerHTML = "Senha fraca. Coloque uma senha com no minimo 8 caracteres.";
        } else if (senha.value == "") {
            senha.style.borderColor = "red";
            spanSenha.innerHTML = "Coloque uma senha.";
        } else {
            senha.style.borderColor = "#77e610f8";
            spanSenha.innerHTML = "";
        }
    }
    senha2.onblur = function () {
        if (senha.value != senha2.value) {
            senha2.style.borderColor = "red";
            spanSenha2.innerHTML = "Senhas diferentes.";
        } else {
            senha2.style.borderColor = "#77e610f8";
            spanSenha2.innerHTML = "";
        }
    }
}
//usuario
var nome = document.getElementById("nome");
if (nome != undefined) {
    nome.onbinput = function () {
        var spanNome = document.getElementById("spanNome");
        if (nome.value.length > 5) {
            nome.style.borderColor = "#77e610f8";
            spanNome.innerHTML = "";
        } else if (nome.value == "") {
            nome.style.borderColor = "red";
        } else {
            nome.style.borderColor = "red";
            spanNome.innerHTML = "";
        }
    }
}
//botao desativado
document.getElementById("form").oninput = function(){
    desativarBotao();
}
//envio
document.getElementById("form").onsubmit = function (e) {
    e.preventDefault();
    if (!verificarCampos() && !verificarSpan()) {
        var data = new FormData($("#form")[0]);
        $.ajax({
            url: "cadastrar.php",
            type: "post",
            data: data,
            processData: false,
            contentType: false,
            dataType: 'json',
            beforeSend: function () {
                $("#botao").html("");
                document.getElementById("botao").setAttribute("uk-spinner", "");
            },
            success: function (response) {
                document.getElementById("botao").removeAttribute("uk-spinner", "");
                $("#botao").html("registrar");
                if (response.codigo == 1) {
                    alertify.alert("Agradecemos sua escolha", response.success.mensagem);
                    alertify.success("Cadastro realizado com sucesso.");
                    UIkit.toggle("#toggle").toggle();
                    document.forms[1].reset();
                    
                } else {
                    var email = document.getElementById("spanEmail");
                    var senha = document.getElementById("spanSenha");
                    var nome = document.getElementById("spanNome");
                    var emailCampo = document.getElementById("email");
                    var senhaCampo = document.getElementById("senha");
                    var nomeCampo = document.getElementById("nome");
                    switch (response.erro.campo) {
                        case "banco":
                            alertify.error(response.erro.mensagem);
                            break;
                        case "senha":
                            senha.innerHTML = response.erro.mensagem;
                            senhaCampo.style.borderColor = "red";
                            break;
                        case "nome":
                            nome.innerHTML = response.erro.mensagem;
                            nomeCampo.style.borderColor = "red";
                            break;
                        case "email":
                            email.innerHTML = response.erro.mensagem;
                            emailCampo.style.borderColor = "red";
                            break;
                    }
                }
            }
        });
    }
}
