<?php
	require_once("Modelo/Usuario.class.php");
	require_once("Modelo/Agencia.class.php");
	class ContaBancaria {
		private $conta;
		private $usuario;
		private $valor;
		private $agencia;
		private $tipoConta;
		
		public function getConta() {
			return $this->conta;
		}
		public function setConta($conta) {
			$this->conta = $conta;
		}
		public function getUsuario() {
			return $this->usuario;
		}
		public function setUsuario($usuario) {
			$this->usuario = $usuario;
		}
		public function getAgencia() {
			return $this->agencia;
		}
		public function setAgencia($agencia) {
			$this->agencia = $agencia;
		}
		public function getValor() {
			return $this->valor;
		}
		public function setValor($valor) {
			$this->valor = $valor;
		}
		public function getTipoConta() {
			return $this->tipoConta;
		}
		public function setTipoConta($tipoConta) {
			$this->tipoConta = $tipoConta;
		}
		function __construct(){
			$this->setUsuario(new Usuario());
			$this->setAgencia(new Agencia());
		}
	}
?>