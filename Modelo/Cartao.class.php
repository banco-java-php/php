<?php

require_once("Modelo/ContaBancaria.class.php");
require_once("Modelo/Usuario.class.php");

class Cartao{
	private $id;
	private $contaBancaria;
	private $usuario;
	private $valor;
	private $tipoCartao;
	private $limite;

	public function getId(){
		return $this->id;
	}
	public function setId($i){
		$this->id = (isset($i)) ? $i :NULL;
	}
	public function getContaBancaria(){
		return $this->contaBancaria;
	}
	public function setContaBancaria($cb){
		$this->contaBancaria = (isset($cb)) ? $cb :NULL;
	}
	public function getUsuario(){
		return $this->usuario;
	}
	public function setUsuario($u){
		$this->usuario = (isset($u)) ? $u :NULL;
	}
	public function getValor(){
		return $this->valor;
	}
	public function setValor($v){
		$this->valor = (isset($v)) ? $v :NULL;
	}
	public function getTipoCartao(){
		return $this->tipoCartao;
	}
	public function setTipoCartao($tC){
		$this->tipoCartao = (isset($tC)) ? $tC :NULL;
	}
	public function getLimite(){
		return $this->limite;
	}
	public function setLimite($l){
		$this->limite = (isset($l)) ? $l :NULL;
	}
	public function __construct(){
		$this->setContaBancaria(new ContaBancaria());
		$this->setUsuario(new Usuario());
	}
}	

  ?>