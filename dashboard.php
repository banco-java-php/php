
<?php
	require_once('Controle/CartaoController.class.php');
	require_once('Controle/ContaBancariaController.class.php');
	require_once('Controle/RegistroController.class.php');
	require_once('Controle/UsuarioController.class.php');

  ?>    
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Interface do Usuário</title>
		<link rel="icon" href="imgs/money.png">
		<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
		<link rel="stylesheet" type="text/css" href="css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="css/alertify.min.css">
	</head>
	<body>
	
	 	
		<?php
		session_start();
		 if(!isset($_SESSION["usuario"])){
			header("Location: index.php");

		}	
		setcookie("addRegistroResult",null);
		setcookie("addContaResult",null);
		setcookie("addCartaoResult",null);
		date_default_timezone_set("America/Fortaleza");
		$data = new DateTime();
		$usu = unserialize($_SESSION['usuario']);
		?>
		<?php

			$registroControle = new RegistroController();
			$regList = $registroControle->consultarRegistros(unserialize($_SESSION['usuario'])->getId());

			$contaControle = new ContaBancariaController();
			$contaList = $contaControle->consultarContasBancarias(unserialize($_SESSION['usuario'])); 

			$cartaoControle = new CartaoController();
			$cartaoList = $cartaoControle->consultarCartoes(unserialize($_SESSION['usuario'])); 
		?>
		<header id="top-head" class="uk-position-fixed">
			<div class="uk-container uk-container-expand " style="background-color: #222;">
				<nav class="uk-navbar uk-light" data-uk-navbar="mode:click; duration: 250">
					<div class="uk-navbar-left">
						<img style='width:30px' src='imgs/money.png'> 
						<a href="dashboard.php">yourMoney</a>
					</div>
					<div class="uk-navbar-right">
						<ul class="uk-navbar-nav">
							<li><a data-uk-icon="icon:  bell" onclick="mostrarAvisos()" title="Avisos" data-uk-tooltip></a></li>
							<li><a href="sair.php" data-uk-icon="icon:  sign-out" title="Sair" data-uk-tooltip></a></li>
							<li><a class="uk-navbar-toggle" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav" title="Menu" data-uk-tooltip></a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
		
		<div id="content" data-uk-height-viewport="expand: true">
			<div class="uk-container uk-container-expand">
				<div class="uk-grid uk-grid-divider uk-grid-medium uk-child-width-1-1 uk-child-width-1-4@l uk-child-width-1-5@xl" data-uk-grid>
					<div>
						<span class="uk-text-small"><span class="uk-margin-small-right uk-text-primary" style="font-size:17px">$</span>Carteira</span>
						
							<?php
								$uC=new UsuarioController();
								$a=$uC->mostrarCarteira($usu->getId());
								if($a[0]->getCarteira()==0.00){
									echo"
									<div class='uk-text-small'>
									Você não possui.
									<h2 class='uk-heading-secondary uk-margin-remove uk-text-primary'><button class='uk-button uk-button-secondary' id='botaoCarteira'>Adicionar</button></h2>";
								}else{
									$c=number_format($a[0]->getCarteira(),2,",",".");
									echo "
									<h1 class='uk-heading-primary uk-margin-remove  uk-text-primary' style='font-size:150%;'>R$ {$c}</h1>
									<div class='uk-text-small'>";
									
								}
							  ?>	

						</div>
						
					</div>
					<div>
						<span class="uk-text-small"><span data-uk-icon="icon:world" class="uk-margin-small-right uk-text-primary"></span>Contas</span>
						<div class="uk-text-small">
							<?php $countContaList = count($contaList); if($countContaList==0){echo"Você não possui.";}else{echo"Você possui {$countContaList}<a href='contas.php'> (Gerenciar)</a>";} ?>
						</div>
						<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button id="botaoConta" class="uk-button uk-button-secondary">Adicionar</button></h2>
					</div>
					<div>
						<span class="uk-text-small"><span data-uk-icon="icon:credit-card" class="uk-margin-small-right uk-text-primary"></span>Cartões de crédito</span>
						<div class="uk-text-small">
							<?php $countCartaoList = count($cartaoList); if($countCartaoList==0){echo"Você não possui.";}else{echo"Você possui {$countCartaoList}<a href='cartoes.php'> (Gerenciar)</a>";} ?>
						</div>
						<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button id="botaoCartao" class="uk-button uk-button-secondary">Adicionar</button></h2>
					</div>
					<div>
						<span class="uk-text-small"><span data-uk-icon="icon:bookmark" class="uk-margin-small-right uk-text-primary"></span>Registros</span>
						<div class="uk-text-small">
							<?php $countRegList = count($regList); if($countRegList==0){echo"Você não possui.";}else{echo"Você possui {$countRegList}<a href='registros.php'> (Ver)</a>";} ?>
						</div>
						<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button id="botaoRegistro" class="uk-button uk-button-secondary">Adicionar</button></h2>
					</div>
					
				</div>
				<hr>
				<div class="uk-grid uk-grid-medium" data-uk-grid>
					
		
					<div class="uk-width-1-2@l">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Últimos registros <a href="registros.php">(Ver Mais)</a></h4></div>
									
								</div>
							</div>
							<div class="uk-card-body uk-overflow-auto">
								
								
							
								
								<table class="uk-table uk-table-divider uk-table-striped uk-table-hover">
									<tr>
										<th></th>
										<th>Título</th>
										<th>Valor</th>
										<th>Meio</th>
										<th>Data</th>
									</tr>
									<?php
										$cont = 0;
										foreach ($regList as $rL) {

											$valor = $rL->getValor();
											$tpRegistro = "plus.png";
											if($rL->getTipoRegistro()=="saida"){
												$valor = $valor*(-1);
												$tpRegistro = "less.png";
											}
											$valor = number_format($valor, 2, ",", ".");
											if($rL->getTipoPagamento()=="carteira"){
												$meio = "Carteira";
											}else if($rL->getTipoPagamento()=="conta"){
												$meio = "Conta Bancaria";
											}else{
												$meio = "Cartão";
											}
											if($cont<5){
												$data2=date('d-m-Y à\s H:i:s',strtotime($rL->getDataRegistro()));
												echo "<tr>";
												echo "<td><img style='width:15px;height:15px' src='imgs/{$tpRegistro}'></td>";
												echo "<td>{$rL->getTitulo()}</td>";
												echo "<td> R$ {$valor}</td>";
												echo "<td>{$meio}</td>";
												echo "<td>{$data2}</td>";
												echo "</tr>";
											}
											$cont++;
										}
							 
									?>
									
								</table>
							</div>
						</div>
					</div>

					<div class="uk-width-1-2@l">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Sua Carteira este mês</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								
								<div class="chart-container">
									<canvas id="chart2"></canvas>
								</div>


							</div>
						</div>
					</div>

					<div class="uk-width-1-1 uk-width-1-3@l uk-width-1-2@xl">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Saída mensal</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								
								<div class="chart-container">
									<canvas id="chart3"></canvas>
								</div>


							</div>
						</div>
					</div>

					<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Entrada mensal</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								
								<div class="chart-container">
									<canvas id="chart4"></canvas>
								</div>
							

							</div>
						</div>
					</div>

					<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Balanço Geral - Mês</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								<div class="chart-container">
									<canvas id="chart5"></canvas>
								</div>
							</div>
						</div>
					</div>

				</div>
				<footer class="uk-section uk-section-small uk-text-center">
					<hr>
					<p class="uk-text-small uk-text-center">© 2019 yourMoney - <a href="">Desenvolvido por Lucas Silva, Lucas Wendel e Rodrigo Silva</a> | Feito com <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a> </p>
				</footer>
			</div>
		</div>

		<div id="offcanvas-nav" data-uk-offcanvas="flip: true; overlay: true">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
				<button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>
				<aside id="left-col" class="uk-light ">
					<div class="left-logo uk-flex uk-flex-middle">
						<img class="custom-logo" src="imgs/money.png" alt="yourMoney"> yourMoney
					</div>
					<div class="left-content-box  content-box-dark">
						<h4 class="uk-text-center uk-margin-remove-vertical text-light"></h4>
						
						<div class="uk-position-relative uk-text-center uk-display-block">
						    <a href="#" class="uk-text-small uk-text-muted uk-display-block uk-text-center" data-uk-icon="icon: triangle-down; ratio: 0.7"><?php echo $usu->getNome() ?></a>
						   
						    <div class="uk-dropdown user-drop" data-uk-dropdown="mode: click; pos: bottom-center; animation: uk-animation-slide-bottom-small; duration: 150">
						    	<ul class="uk-nav uk-dropdown-nav uk-text-left">
										<li><a href="sair.php"><span data-uk-icon="icon: sign-out"></span> Sair</a></li>
							    </ul>
						    </div>
						   
						</div>
					</div>
					
					<div class="left-nav-wrap">
						<ul class="uk-nav uk-nav-default uk-nav-parent-icon" data-uk-nav>
							<li class="uk-nav-header">PÁGINAS</li>
							<li><a href="dashboard.php"><span data-uk-icon="icon: home" class="uk-margin-small-right"></span>Início</a></li>
							<li><a href="registros.php"><span data-uk-icon="icon: bookmark" class="uk-margin-small-right"></span>Registros</a></li>
							<li><a href="contas.php"><span data-uk-icon="icon: world" class="uk-margin-small-right"></span>Contas Bancárias</a></li>
							<li><a href="cartoes.php"><span data-uk-icon="icon: credit-card" class="uk-margin-small-right"></span>Cartões de Crédito</a></li>
							
						</ul>
						<div class="left-content-box uk-margin-top">
							
								<h5>Sobre o <i>yourMoney</i></h5>
								Com <i>yourMoney</i>, você pode administrar as suas transações, permitindo você facilitar a sua vida.
							
						</div>
						
					</div>
					
				</aside>
			</div>
		</div>
	
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<script src="js/uikit.min.js"></script>
		<script src="js/uikit-icons.min.js"></script>
		<script src="js/alertify.min.js"></script>
		<script type="text/javascript" src="js/input-mask.js"></script>
		<script type="text/javascript" src="js/maskMoney.min.js"></script>

		<?php 
			$contasCartao = "";

			$contas = "";
			for($i=0;$i<count($contaList);$i++){
				$nConta = $contaList[$i]->getConta();
				$agenciaConta = $contaList[$i]->getAgencia()->getNome(); 
				$contas = $contas . "<option value='{$nConta}'>{$agenciaConta} - {$nConta}</option>";
				if($contaList[$i]->getTipoConta()=="corrente"){
					$contasCartao = $contasCartao . "<option value='{$nConta}'>{$agenciaConta} - {$nConta}</option>";
				}
			}

			$cartoes = "";
			for($i=0;$i<count($cartaoList);$i++){
				$nCartao = substr($cartaoList[$i]->getId(),-4,4);
				$nCartao = "**** **** **** {$nCartao}";
				$apenasDebito = "";

				$agencia = ($cartaoList[$i]->getContaBancaria()->getConta()!=null) ?$cartaoList[$i]->getContaBancaria()->getAgencia()->getNome() : "Sem Banco";
				if($cartaoList[$i]->getLimite()==null || $cartaoList[$i]->getContaBancaria()->getConta()==null){
					$apenasDebito = "class='opcoesCredito'";
				}
				$cartoes = $cartoes . "<option {$apenasDebito} value='{$cartaoList[$i]->getId()}'>{$agencia} - {$nCartao}</option>";
			}

			$registrosFixos = "";
			for($i=0;$i<count($regList);$i++){
				if($regList[$i]->getRepeticao()=="fixa" && $regList[$i]->getDataVencimento()!=null){
					$idReg = $regList[$i]->getId();
					$titulo = $regList[$i]->getTitulo();
					$registrosFixos = $registrosFixos . "<option value='{$idReg}'>{$titulo}</option>";
				}
			}

			$parcelamentos = "";
			for($i=0;$i<count($regList);$i++){
				if($regList[$i]->getRepeticao()=="parcelamento" && $regList[$i]->getDataVencimento()!=null){
					$idReg = $regList[$i]->getId();
					$titulo = $regList[$i]->getTitulo();
					$parcelamentos = $parcelamentos . "<option value='{$idReg}'>{$titulo}</option>";
				}
			}
			$carteiraData = [];
			$diaHoje = (integer)$data->format("d");
			$mesHoje = (integer)$data->format("m");
			$anoHoje = (integer)$data->format("Y");
			$val = (float)$uC->mostrarCarteira($usu->getId())[0]->getCarteira();
			for($i=$diaHoje-1;$i>=0;$i--){

				for($y=0;$y<count($regList);$y++){
					$dt = new DateTime($regList[$y]->getDataOperacao());
					if((integer)$dt->format("Y")==$anoHoje &&(integer)$dt->format("m")==$mesHoje && (integer)$dt->format("d")==($i+2) && ($regList[$y]->getTipoPagamento()=="carteira")){
						if($regList[$y]->getTipoRegistro()=="entrada"){
							$val = $val - $regList[$y]->getValor();
						}else{
							$val = $val + $regList[$y]->getValor();
						}
					}
				}

				array_unshift($carteiraData,$val);
				
				
			}
			$carteiraDataString = implode("|", $carteiraData);
			$saidaTotalData = [0,0,0];
			$entradaTotalData = [0,0,0];
			foreach ($regList as $rL) {
				$dtReg = new DateTime($rL->getDataOperacao());
				if((integer)$dtReg->format("m")==(integer)$data->format("m") && (integer)$dtReg->format("Y")==(integer)$data->format("Y")){
					if($rL->getTipoRegistro()=="saida"){
						if($rL->getTipoPagamento()=="carteira"){
							$saidaTotalData[0]+=$rL->getValor();
						}else if($rL->getTipoPagamento()=="conta") {
							$saidaTotalData[1]+=$rL->getValor();
						}else{
							$saidaTotalData[2]+=$rL->getValor();
						}

					}else{
						if($rL->getTipoPagamento()=="carteira"){
							$entradaTotalData[0]+=$rL->getValor();
						}else if($rL->getTipoPagamento()=="conta") {
							$entradaTotalData[1]+=$rL->getValor();
						}else{
							$entradaTotalData[2]+=$rL->getValor();
						}

					}
				}
			}
			$saidaTotalDataString = implode("|", $saidaTotalData);
			$entradaTotalDataString = implode("|", $entradaTotalData);


			$avisos = "<div class='uk-grid-small' uk-grid>";
			$avisosPrioridadeAlta = "";
			$avisosPrioridadeMedia = "";
			$avisosPrioridadeBaixa = "";
			$countAvisos = 0;
			foreach ($regList as $rL) {
				if($rL->getDataVencimento()){
					$dtVenReg = null;
					if($rL->getRepeticao()=="fixa"){
						$dtVenReg = new DateTime($rL->getDataVencimento());
					}else{
						$dtVenReg = new DateTime($rL->getDataOperacao());
						$dtVenReg->modify("+1 month");
					}
					$difDatas = $dtVenReg->diff($data);
					if($difDatas->invert==0 && $difDatas->days!=0){
						$avisosPrioridadeAlta.= "".
						"<div class='uk-width-1-1'>".
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>".
								"<div class='uk-card-header'>".
									"<div class='uk-grid uk-grid-small'>".
										"<div class='uk-width-auto'><h4>Pagamento Vencido: {$rL->getTitulo()}</h4></div>".
									"</div>".
								"</div>".
								"<div class='uk-card-body'>".
									"Você possui um pagamento vencido referente ao dia <b>{$dtVenReg->format('d-m-Y')}</b> !".
								"</div>".
							"</div>".
						"</div>";
						$countAvisos++;
					}else if($difDatas->days<1 && (integer)$dtVenReg->format("d")!=(integer)$data->format("d")+1){
						$avisosPrioridadeMedia.= "".
						"<div class='uk-width-1-1'>".
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>".
								"<div class='uk-card-header'>".
									"<div class='uk-grid uk-grid-small'>".
										"<div class='uk-width-auto'><h4>A vencer hoje: {$rL->getTitulo()}</h4></div>".
									"</div>".
								"</div>".
								"<div class='uk-card-body'>".
									"Um pagamento vence hoje <b>{$dtVenReg->format('d-m-Y')}</b>! ".
								"</div>".
							"</div>".
						"</div>";
						$countAvisos++;
					}else if($difDatas->days<7){
						$avisosPrioridadeBaixa.= "".
						"<div class='uk-width-1-1'>".
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>".
								"<div class='uk-card-header'>".
									"<div class='uk-grid uk-grid-small'>".
										"<div class='uk-width-auto'><h4>Vencimento próximo: {$rL->getTitulo()}</h4></div>".
									"</div>".
								"</div>".
								"<div class='uk-card-body'>".
									"Um pagamento está próximo de seu vencimento, que está previsto para o dia <b>{$dtVenReg->format('d-m-Y')}</b>.".
								"</div>".
							"</div>".
						"</div>";
						$countAvisos++;
					}
			
				}
			}
			if($countAvisos==0){
				$avisos.="Nenhum aviso por agora! :)";
			}
			$avisos.= $avisosPrioridadeAlta . $avisosPrioridadeMedia . $avisosPrioridadeBaixa;
			$avisos.= "</div>";



		?>
		<script>
			var contasCartao = "<?php echo $contasCartao ?>";
			var contas = "<?php echo $contas ?>";
			var cartoes = "<?php echo $cartoes ?>";
			var parcelamentos = "<?php echo $parcelamentos ?>";
			var registrosFixos = "<?php echo $registrosFixos ?>";
			var cData = "<?php echo $carteiraDataString ?>";
			var carteiraData = cData.split("|");
			var labels = [];
			for(i=0;i<carteiraData.length;i++){
				if(i==0){
					labels[i] = "dia 1";
				}else if(i==7){
					labels[i] = "dia 8";
				}else if(i==14){
					labels[i] = "dia 15";
				}else if(i==21){
					labels[i] = "dia 22";
				}else if(i==28){
					labels[i] = "dia 29";
				}else if(i==(carteiraData.length)-1) {
					labels[i] = "hoje "+(i+1);
				}else{
					labels[i] = "";
				}
			}
			var saidaTotalDataString = "<?php echo $saidaTotalDataString ?>";
			var saidaTotalData = saidaTotalDataString.split("|");
			var entradaTotalDataString = "<?php echo $entradaTotalDataString ?>";
			var entradaTotalData = entradaTotalDataString.split("|");

			var avisos = "<?php echo $avisos ?>";

		</script>
		<script src="js/Chart.min.js"></script>
		<script src="js/chartScripts.js"></script>
		<script src="js/addForms.js"></script>
		<script src="js/javinha.js"></script>
		<script>
		var botConta = document.getElementById("botaoConta");
		var botCartao = document.getElementById("botaoCartao");
		var botRegistro = document.getElementById("botaoRegistro");
		var botCarteira= document.getElementById("botaoCarteira");
		if(!alertify.myAlert){
			alertify.dialog('myAlert',function(){
			return{
				main:function(message){
			        this.message = message;
			      },
			      setup:function(){
			          return { 
			            focus: { element:0 },
		                options:{
		                    maximizable:false,
		                    resizable:true,
		                    movable:false
		                }
			          };
			      },
			      prepare:function(){
			        this.setContent(this.message);
			      }
			  }});
			}
		botConta.onclick = function(){
			alertify.myAlert(formConta).resizeTo('40%','80%').setHeader('<h2>Adicionar Conta Bancária</h2>');
		}
		botCartao.onclick = function(){
			alertify.myAlert(formCartao).resizeTo('40%','80%').setHeader('<h2>Adicionar Cartão de Crédito</h2>');
		}
		botRegistro.onclick = function(){
			alertify.myAlert(formRegistro).resizeTo('40%','80%').setHeader('<h2>Adicionar registro</h2>');
		}
		botCarteira.onclick = function(){
			alertify.myAlert(carteira).resizeTo('40%','80%').setHeader('<h2>Adicionar Carteira</h2>');
		}
		
		</script>
		<?php
			if(isset($_COOKIE['addRegistroResult']) ){

				if($_COOKIE['addRegistroResult']=="1"){
					echo "<script>alertify.success('Registro adicionado com sucesso!')</script>";
				}else{
					echo "<script>alertify.error('Erro ao efetuar registro!')</script>";
					echo "<script>alertify.myAlert(formRegistro).resizeTo('40%','80%').setHeader('<h2>Enviar registro</h2>');</script>";	
				}

			}
			if(isset($_COOKIE['addContaResult'])){
				if($_COOKIE['addContaResult']=="1"){
					echo "<script>alertify.success('Conta adicionada com sucesso!')</script>";	
				}else{
					echo "<script>alertify.error('Erro ao adicionar conta!')</script>";
					echo "<script>alertify.myAlert(formRegistro).resizeTo('40%','80%').setHeader('<h2>Adicionar conta</h2>');</script>";
				}

			}
			if(isset($_COOKIE['addCartaoResult'])){
				if($_COOKIE['addCartaoResult']=="1"){
					echo "<script>alertify.success('Cartão adicionado com sucesso!')</script>";
				}else{
					echo "<script>alertify.error('Erro ao adicionar cartão!')</script>";
					echo "<script>alertify.myAlert(formRegistro).resizeTo('40%','80%').setHeader('<h2>Adicionar cartão</h2>');</script>";
				}
				
			}
			
		  ?>
		
	</body>
</html>
