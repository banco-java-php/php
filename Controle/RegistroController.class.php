<?php
	require_once("Controle/CartaoController.class.php");
	require_once("Controle/ContaBancariaController.class.php");
	require_once("Modelo/Registro.class.php");
	require_once("Conection.class.php");

	class RegistroController{
		public function adicionarRegistros($reg,$id,$forma,$id_Forma){
			$conection=new Conection("lib/mysql.ini");
			$titulo=$reg->getTitulo();
			$valor=$reg->getValor();
			$desc=$reg->getDesc();
			$tipoCartao=$reg->getTipoCartao();
			$tipoPagamento=$reg->getTipoPagamento();
			$tipoRegistro=$reg->getTipoRegistro();
			$dataRegistro=$reg->getDataRegistro();
			$dataOperacao=$reg->getDataOperacao();
			$dataVencimento=$reg->getDataVencimento();
			$repeticao=$reg->getRepeticao();

			$id_conta = null;
			$id_cartao = null;
			if($forma=="conta") {
				$id_conta = $id_Forma;
			}else if($forma=="cartao"){
				$id_cartao = $id_Forma;
			}
			$idU=$id->getId();
			$sql="INSERT INTO Registro(titulo,valor,descricao,repeticao,tipoCartao,tipoPagamento,tipoRegistro,dataRegistro,dataOperacao,dataVencimento,id_conta,id_cartao,id_usuario) VALUES (:titulo,:valor,:descricao,:repeticao,:tipoCartao,:tipoPagamento,:tipoRegistro,:dataRegistro,:dataOperacao,:dataVencimento,:id_conta,:id_cartao,:id_usuario)";
			
			$comando=$conection->getConection()->prepare($sql);
			$comando->bindParam(":titulo",$titulo);
			$comando->bindParam(":valor",$valor);
			$comando->bindParam(":descricao",$desc);
			$comando->bindParam(":repeticao",$repeticao);
			$comando->bindParam(":tipoCartao",$tipoCartao);
			$comando->bindParam(":tipoPagamento",$tipoPagamento);
			$comando->bindParam(":tipoRegistro",$tipoRegistro);
			$comando->bindParam(":dataRegistro",$dataRegistro);
			$comando->bindParam(":dataOperacao",$dataOperacao);
			$comando->bindParam(":dataVencimento",$dataVencimento);
			$comando->bindParam(":id_conta",$id_conta);
			$comando->bindParam(":id_cartao",$id_cartao);
			$comando->bindParam(":id_usuario",$idU);
			if($comando->execute()){
				$conection->__destruct();
				return true;
			}else{
				$conection->__destruct();
				return false;
			}

		}

		public function consultarRegistros($id){
			$conection= new Conection("lib/mysql.ini");
			$sql="SELECT * FROM Registro WHERE id_usuario={$id} ORDER BY dataRegistro DESC";
			$comando=$conection->getConection()->prepare($sql);
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			if($resultado!=null){
				foreach($resultado as $item){
					$registro= new Registro();
					$registro->setId($item->id);
					$registro->setTitulo($item->titulo);
					$registro->setValor($item->valor);
					$registro->setDesc($item->descricao);
					$registro->setRepeticao($item->repeticao);
					$registro->setTipoCartao($item->tipoCartao);
					$registro->setTipoPagamento($item->tipoPagamento);
					$registro->setTipoRegistro($item->tipoRegistro);
					$registro->setDataRegistro($item->dataRegistro);
					$registro->setDataOperacao($item->dataOperacao);
					$registro->setDataVencimento($item->dataVencimento);
					if(isset($item->id_conta)){
						$contaC = new ContaBancariaController();
						$conta = $contaC->consultarContasBancariasById($item->id_conta);
						$registro->setConta($conta);
					}
					if(isset($item->id_cartao)){
						$cartaoC = new CartaoController();
						$cartao = $cartaoC->consultarCartoesById($item->id_cartao);
						$registro->setCartao($cartao);
					}
					
					array_push($lista, $registro);
				}
			}
			$conection->__destruct();
			return $lista;
		}

		public function consultarRegistrosFiltro($id,$filtro,$valor){
			$conection= new Conection("lib/mysql.ini");
			$sql="SELECT * FROM Registro WHERE id_usuario={$id} and {$filtro}  LIKE '%{$valor}%'  ORDER BY dataRegistro DESC";
			$comando=$conection->getConection()->prepare($sql);
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			if($resultado!=null){
				foreach($resultado as $item){
					$registro= new Registro();
					$registro->setId($item->id);
					$registro->setTitulo($item->titulo);
					$registro->setValor($item->valor);
					$registro->setDesc($item->descricao);
					$registro->setRepeticao($item->repeticao);
					$registro->setTipoCartao($item->tipoCartao);
					$registro->setTipoPagamento($item->tipoPagamento);
					$registro->setTipoRegistro($item->tipoRegistro);
					$registro->setDataRegistro($item->dataRegistro);
					$registro->setDataOperacao($item->dataOperacao);
					$registro->setDataVencimento($item->dataVencimento);
					array_push($lista, $registro);
				}
			}
			$conection->__destruct();
			return $lista;
		}

		public function consultarRegistrosById($id){
			$conection= new Conection("lib/mysql.ini");
			$sql="SELECT * FROM Registro WHERE id={$id}";
			$comando=$conection->getConection()->prepare($sql);
			$comando->execute();
			$res = $comando->fetch();
			$registro = new Registro();
			if($res){
				$registro->setId($res->id);
				$registro->setTitulo($res->titulo);
				$registro->setValor($res->valor);
				$registro->setDesc($res->descricao);
				$registro->setRepeticao($res->repeticao);
				$registro->setTipoCartao($res->tipoCartao);
				$registro->setTipoPagamento($res->tipoPagamento);
				$registro->setTipoRegistro($res->tipoRegistro);
				$registro->setDataRegistro($res->dataRegistro);
				$registro->setDataOperacao($res->dataOperacao);
				$registro->setDataVencimento($res->dataVencimento);

			}
			$conection->__destruct();	
			return $registro;
		}

		public function atualizarVencimento($id){
			$conection= new Conection("lib/mysql.ini");
			$sql="UPDATE Registro SET dataVencimento=NULL WHERE id={$id}";
			$comando=$conection->getConection()->prepare($sql);
			$resultado = false;
			if($comando->execute()){
				$resultado = true;
			}
			return $resultado;
		}	


	}	


  ?>