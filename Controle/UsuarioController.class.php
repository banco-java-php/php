<?php
require_once("Conection.class.php");
require_once("Modelo/Usuario.class.php");

class UsuarioController{

	public function verificar($user){
	 	$conection=new Conection('lib/mysql.ini');
	 	$email=$user->getEmail();
	 	$senha=$user->getSenha();
	 	$sql="SELECT * FROM  Usuario WHERE email=:email";
	 	$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam(':email', $email);
		$comando->execute();
		$users = $comando->fetch();
			if(!$users){
				header("Location: login.php");
			}else{
				$senhaCriptografada = base64_decode($users->senha);
				$Cifra =  'AES-256-CBC';
				$TextoCifrado = mb_substr($senhaCriptografada, openssl_cipher_iv_length($Cifra), null, '8bit');
				

				$Chave =  "I�qeBS���yڭJEFf8��/3h ���";
				$IV = mb_substr($senhaCriptografada, 0, openssl_cipher_iv_length($Cifra), '8bit');

				$TextoClaro = openssl_decrypt($TextoCifrado, $Cifra, $Chave, OPENSSL_RAW_DATA, $IV);
				if($senha==$TextoClaro){
					$user = new Usuario();
					$user->setId($users->id);
					$user->setNome($users->nome);
					$user->setSenha($TextoClaro);
					$user->setEmail($users->email);
					$user->setCarteira($users->carteira);
					$_SESSION['usuario']=serialize($user);
					header("Location: dashboard.php");
				}else{
					header("Location: login.php");
				}
			}
		$conection->__destruct();	
	
	}
	public function adicionarCarteira($user,$id){
		$conection= new Conection("lib/mysql.ini");
		$carteira=$user->getCarteira();
		$sql="UPDATE Usuario SET carteira=:carteira+carteira WHERE id={$id}";
		$comando=$conection->getConection()->prepare($sql);
		$comando->bindParam(":carteira",$carteira);
		if($comando->execute()){
			header("Location: dashboard.php");
		}else{
			echo "Error";
		}
		$conection->__destruct();
	}
	public function mostrarCarteira($id){
		$conection= new Conection("lib/mysql.ini");
		$sql="SELECT carteira FROM Usuario WHERE id={$id}";
		$comando=$conection->getConection()->prepare($sql);
		$lista=[];
		$comando->execute();
		$resultado=$comando->fetchAll();
		if($resultado!=null){
			foreach($resultado as $item){
				$carteira= new Usuario();
				$carteira->setCarteira($item->carteira);				
				array_push($lista, $carteira);
			}
		}else{
			echo "nop";
		}
		$conection->__destruct();	
		return $lista;
	}

	public function atualizarCarteira($id,$carteira) {
		$conection= new Conection("lib/mysql.ini");
		$sql="UPDATE Usuario SET carteira=carteira+{$carteira} WHERE id={$id}";
		$comando=$conection->getConection()->prepare($sql);
		$res = $comando->execute();

		$conection->__destruct();
		return $res;
	}
	 public function inserir($usuario){
        $resultado = false;
        try{
            $pdo = new Conection("lib/mysql.ini");
            $sql = "INSERT INTO usuario(nome, senha, email) VALUES (:nome, :senha, :email);";
            $prepared = $pdo->getConection()->prepare($sql);
            $prepared->bindValue(":nome", $usuario->getNome());
            $prepared->bindValue(":senha", $usuario->getSenha());
            $prepared->bindValue(":email", $usuario->getEmail());
            if($prepared->execute()){
                $resultado = true;
            }
            $pdo->__destruct();
        }catch(Exception $e){
            $ponto = fopen("log/log.txt","w");
            $erro = "Erro: {$e->getMessage()}\nLinha: {$e->getLine()}\nArquivo: {$e->getFile()}";
            fwrite($ponto, $erro);
        }
        return $resultado;
    }
    public function verificarEmail($email){
        $resultado = false;
        try{
            $pdo = new Conection("lib/mysql.ini");
            $prepared = $pdo->getConection()->prepare("SELECT * FROM usuario WHERE email=:email");
            $prepared->bindValue(":email",$email);
            if($prepared->execute()){
                if($prepared->rowCount() == 1){
                    $resultado = true;
                }
            }
            $pdo->__destruct();
        }catch(Exception $e){
            $ponto = fopen("log/log.txt","w");
            $erro = "Erro: {$e->getMessage()}\nLinha: {$e->getLine()}\nArquivo: {$e->getFile()}";
            fwrite($ponto, $erro);
        }
        return $resultado;
    }

	public function atualizarSenha($senha) {
    	$conection= new Conection("lib/mysql.ini");
		$sql="UPDATE Usuario SET senha='{$senha}' WHERE id=1";
		$comando=$conection->getConection()->prepare($sql);
		$res = $comando->execute();

		$conection->__destruct();
		return $res;
    }

}	
 ?>
