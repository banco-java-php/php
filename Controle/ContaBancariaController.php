<?php
	
require_once("Conection.class.php");
require_once("Modelo/Usuario.class.php");
require_once("Modelo/Agencia.class.php");
require_once("Controle/AgenciaController.php");
require_once("Modelo/ContaBancaria.class.php");

class ContaBancariaController{
	public function consultarContasBancarias($id){
		$conection=new Conection('lib/mysql.ini');
		$idU=$id->getId();
		$sql="SELECT * FROM  ContaBancaria WHERE id_usuario={$idU}";
		$comando=$conection->getConection()->prepare($sql);
		$comando->execute();
		$list = [];
		if($comando){
			$res = $comando->fetchAll();
			foreach($res as $item){
				$conta = new ContaBancaria();
				$conta->setConta($item->numeroConta);
				$conta->setValor($item->valor);
				$conta->setTipoConta($item->tipoConta);
				$usuario= new Usuario();
				$usuario->setId($id);
				$conta->setUsuario($usuario);
				$aC= new AgenciaController();
				$age = new Agencia();
				$age = $aC->consultarAgencia($item->id_agencia);
				$conta->setAgencia($age);
				array_push($list, $conta);
			}
		}
		$conection-> __destruct();
		return $list;
	}
}

  ?>