<?php
	
	require_once("Modelo/Registro.class.php");
	require_once("Conection.class.php");

	class RegistroController{
		public function consultarRegistrosLimite($id){
			$conection= new Conection("lib/mysql.ini");
			$idU=$id->getId();
			$sql="SELECT * FROM Registro WHERE id_usuario={$idU}";
			$comando=$conection->getConection()->prepare($sql);
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			if($resultado!=null){
				foreach($resultado as $item){
					$registro= new Registro();
					$registro->setId($item->id);
					$registro->setTitulo($item->titulo);
					$registro->setValor($item->valor);
					$registro->setDesc($item->descricao);
					$registro->setFixo($item->fixo);
					$registro->setTipoCartao($item->tipoCartao);
					$registro->setTipoRegistro($item->tipoRegistro);
					$registro->setDataRegistro($item->dataRegistro);
					$registro->setDataOperacao($item->dataOperacao);
					$registro->setDataVencimento($item->dataVencimento);
					array_push($lista, $registro);
				}
			}
			$conection->__destruct();
			return $lista;
		}		
	}	


  ?>