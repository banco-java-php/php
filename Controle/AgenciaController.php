<?php

	require_once("Modelo/Agencia.class.php");
	require_once("Conection.class.php");

	class AgenciaController{
		public function consultarAgencia($id){
			$conection= new Conection("lib/mysql.ini");
			$sql="SELECT * FROM  Agencia WHERE id=:id";
			$comando=$conection->getConection()->prepare($sql);
			$comando->bindParam(":id",$id);
			$comando->execute();
			$resultado=$comando->fetch();
			$agencia= new Agencia();
			if($resultado){
				$agencia->setId($resultado->id);
				$agencia->setNome($resultado->nome);
			}
			$conection->__destruct();
			return $agencia;
		}
	}

  ?>