DROP SCHEMA IF EXISTS Financa;
CREATE SCHEMA Financa;
USE Financa;
CREATE TABLE IF NOT EXISTS Usuario 
(
	id int primary key auto_increment,
	nome varchar(30) not null,
	senha varchar(255) not null,
	email varchar(60) not null,
	carteira double(15,2) not null
);
CREATE TABLE IF NOT EXISTS Agencia 
(
	id int primary key auto_increment,
	nome varchar(30) not null
);
CREATE TABLE IF NOT EXISTS ContaBancaria
(
	numeroConta int primary key,
	valor double(15,2),
	tipoConta varchar(12),
	id_usuario int not null,
	id_agencia int not null,
	foreign key (id_usuario) references Usuario(id),
	foreign key (id_agencia) references Agencia(id)
);
CREATE TABLE IF NOT EXISTS Cartao 
(
	id int primary key auto_increment,
	valor double(15,2) not null,
	limite double(15,2),
	id_usuario int not null,
	id_conta int,
	foreign key (id_usuario) references Usuario(id),
	foreign key (id_conta) references ContaBancaria(numeroConta)
);
CREATE TABLE IF NOT EXISTS Registro
(
	id int primary key auto_increment,
	titulo varchar(30) not null,
	valor double(15,2) not null,
	descricao text,
	repeticao varchar(20) not null,
	tipoCartao varchar(15),
	tipoPagamento varchar(50) not null,
	tipoRegistro varchar(15) not null,
	dataRegistro datetime not null,
	dataOperacao date,
	dataVencimento date,
	id_conta int,
	id_cartao int,
	id_usuario int not null,
	foreign key (id_conta) references ContaBancaria(numeroConta),
	foreign key (id_cartao) references Cartao(id),
	foreign key (id_usuario) references Usuario(id)
);
INSERT INTO Agencia(id,nome) VALUES(1,"Banco do Brasil");
INSERT INTO Agencia(id,nome) VALUES(2,"Santander");
INSERT INTO Agencia(id,nome) VALUES(3,"Caixa Economica Federal");
INSERT INTO Agencia(id,nome) VALUES(4,"Itau");
INSERT INTO Agencia(id,nome) VALUES(5,"Bradesco"); 
