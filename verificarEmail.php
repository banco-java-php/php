<?php
    require_once("Controle/UsuarioController.class.php");
    $user = new UsuarioController();
    $regex = "/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/";
    if(!preg_match($regex, $_GET['email'])){
        echo "Email inválido";
    }else if($user->verificarEmail($_GET['email'])){
        echo "Email já cadastrado";
    }else{
        echo "";
    }
?>